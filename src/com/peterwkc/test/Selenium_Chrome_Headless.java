package com.peterwkc.test;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Selenium_Chrome_Headless {

	public Selenium_Chrome_Headless() {
	}
	
	public static void main (String args[]) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\peter\\chromedriver.exe");
	 
		ChromeOptions ch = new ChromeOptions();
		ch.addArguments("headless");
		ch.addArguments("window-size=1200x600");
		
		WebDriver driver = new ChromeDriver(ch);
		
		driver.get("http://www.google.lk");
		driver.findElement(By.name("q")).sendKeys("Selenium");
		driver.findElement(By.name("btnK")).click();
		driver.findElement(By.linkText("Selenium - Web Browser Automation")).click();
		String strTitle = driver.getTitle();
		System.out.print("test done");
	}

}
